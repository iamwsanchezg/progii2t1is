package views;

import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class MenuFrame extends WFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3166897613436795561L;
	public MenuFrame(Frame parentFrame) {
		super(parentFrame);
		initComponents();
	}
	public void initComponents() {
	    // Se instancia un objeto de tipo Acelerador de Teclado
	    MenuShortcut miAcelerador = 
	        new MenuShortcut( KeyEvent.VK_K,true );
	
	    // Se instancian varios objetos de tipo Elementos de Menu 
	    MenuItem primerElementoDeA = new MenuItem(
	        "Primer Elemento del Menu A",miAcelerador );
	    MenuItem segundoElementoDeA = new MenuItem(
	        "Segundo Elemento del Menu A" );
	    MenuItem primerElementoDeB = new MenuItem(
	        "Primer Elemento del Menu B" );
	    MenuItem segundoElementoDeB = new MenuItem(
	        "Segundo Elemento del Menu B" );
	    MenuItem tercerElementoDeB = new MenuItem(
	        "Tercer Elemento del Menu B" );
	
	    // Se instancia un objeto ActionListener y se registra sobre los
	    // objetos MenuItem
	    primerElementoDeA.addActionListener( new MiGestorDeMenu() );
	    segundoElementoDeA.addActionListener( new MiGestorDeMenu() );
	    primerElementoDeB.addActionListener( new MiGestorDeMenu() );
	    segundoElementoDeB.addActionListener( new MiGestorDeMenu() );
	    tercerElementoDeB.addActionListener( new MiGestorDeMenu() );
	
	    // Se instancian dos objetos de tipo Menu y se les a�aden los
	    // objetos MenuItem
	    Menu menuA = new Menu( "Menu A" );
	    menuA.add( primerElementoDeA );
	    menuA.add( segundoElementoDeA );
	
	    Menu menuB = new Menu( "Menu B" );
	    menuB.add( primerElementoDeB );
	    menuB.add( segundoElementoDeB );
	    menuB.add( tercerElementoDeB );  
	
	    // Se instancia una Barra de Menu y se le a�aden los Menus
	    MenuBar menuBar = new MenuBar();
	    menuBar.add( menuA );  
	    menuBar.add( menuB );
	
	    // Se le asocia el objeto MenuBar.
	    // Observese que esta no es la tipico invocacion del metodo
	    // add(), sino que es una forma especial de invocar
	    // al metodo necesaria para poder asociar un objeto Barra de Menu
	    // a un objeto Frame
	    // Esto no es el metodo add(), como se podria esperar
	    setMenuBar( menuBar );
	}

}

//Clase para instanciar un objeto ActionListener que se registra
//sobre los elementos del menu
class MiGestorDeMenu implements ActionListener {
 public void actionPerformed( ActionEvent evt ) {
     // Presenta en pantalla el elemento que ha generado el evento
     // de tipo Action
     System.out.println( evt.getSource() );
     }
 }
