package views;

import java.awt.Button;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import misc.event.FormInterface;
public class MainFrame extends Frame implements FormInterface{
	/**
	 * @author William S�nchez 
	 */
	private static final long serialVersionUID = -5771184031903617984L;

	private Button button1;
	private Button button2;
	private Button button3;
	private Button button4;
	private Button button5;
	private Button button6;
	private Frame instance;
	public MainFrame() {
		initComponents();
		instance = this;
	}
	@Override
	public void initComponents() {
		setLayout(null);
		button1 = new Button("Componentes");
		button1.setBounds(80, 50, 100, 32);

		button2 = new Button("Gr�ficos");
		button2.setBounds(210, 50, 100, 32);

		button3 = new Button("Im�genes");
		button3.setBounds(80, 100, 100, 32);

		button4 = new Button("C�lculos");
		button4.setBounds(210, 100, 100, 32);

		button5 = new Button("Browser");
		button5.setBounds(80, 150, 100, 32);
		
		button6 = new Button("Men�");
		button6.setBounds(210, 150, 100, 32);

		add(button1);
		add(button2);
		add(button3);
		add(button4);
		add(button5);
		add(button6);
		
		setSize(390,200);
		this.setResizable(false);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		button1.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				DemoComponent dm = new DemoComponent(instance);
				dm.showForm();
			}
		});

		button2.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				PloterFrame pf = new PloterFrame(instance);
				pf.showForm();
			}
		});

		button3.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				ImageBrowser ib = new ImageBrowser(instance);
				ib.showForm();
			}
		});

		button4.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				MathFrame mf = new MathFrame(instance);
				mf.showForm();
			}
		});

		button5.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				FileBrowser fb = new FileBrowser(instance);
				fb.showForm();
			}
		});
		
		button6.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				MenuFrame mf = new MenuFrame(instance);
				mf.showForm(true);
			}
		});
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub

	}

	@Override
	public void showForm() {
		setVisible(true);
		setLocationRelativeTo(null);
		toFront();
	}
	@Override
	public void showForm(boolean maximize) {
		// TODO Auto-generated method stub
		
	}

}
