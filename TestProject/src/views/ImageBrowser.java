package views;

import java.awt.Button;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FilenameFilter;

import views.components.panels.ImageViewer;

public class ImageBrowser extends WFrame {
	/**
	 * @author William S�nchez 
	 */
	private static final long serialVersionUID = -4287385835715953221L;
	private Button button1;
	private ImageViewer viewer;
	private FileDialog fileDialog;
	public ImageBrowser(Frame parentFrame) {
		super(parentFrame);	
		initComponents();
	}
	@Override
	public void initComponents() {
		setLayout(null);
		fileDialog = new FileDialog(this,"Examinar...",FileDialog.LOAD);
		fileDialog.setFilenameFilter(new FilenameFilter() {
		      @Override
		      public boolean accept(File dir, String name) {
		          return false || name.toLowerCase().endsWith(".jpg")
		              || name.toLowerCase().endsWith(".png")
		              || name.toLowerCase().endsWith(".gif");
		        }
		    });
		viewer = new ImageViewer();
		viewer.setBounds(20,20,350,175);
		button1 = new Button("Examinar");
		button1.setBounds(125,200,100,32);
		add(viewer);
		add(button1);
		viewer.LoadImage("/resources/images/java-logo.jpg", true);		
		setSize(390,250);
		
		button1.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				browse();
			}
		});
	}
	public void browse() {
		fileDialog.setVisible(true);
		if(fileDialog.getFile()!=null) {
			String file = fileDialog.getDirectory() + fileDialog.getFile();    	
    		viewer.LoadImage(file, false);	
    	}
	}
}
