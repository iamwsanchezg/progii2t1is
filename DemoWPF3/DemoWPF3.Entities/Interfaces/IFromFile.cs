﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoWPF3.Entities.Interfaces
{
    public interface IFromFile<T>
    {
        T FromXml(string filepath);
        T FromJson(string filepath);
        T FromBinary(string filepath);
    }

}
