﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoWPF3.Entities.Interfaces
{
    public interface IForm<T>
    {
        void SetData(T data);
        T GetData();
    }
}
