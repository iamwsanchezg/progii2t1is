﻿using Mailer.Views.UserControls;
using System.Windows;

namespace Mailer.Views.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            InitTabControl();
        }

        public void InitTabControl()
        {
            EmailTabItem.Content = new MailEditor();
            AccountTabItem.Content = new AccountConfiguration();
            //AccountTabItem.DataContext = new AccountConfigurationViewModel();
            //TestTabItem.DataContext = new TestConfigurationViewModel();
        }
    }
}
