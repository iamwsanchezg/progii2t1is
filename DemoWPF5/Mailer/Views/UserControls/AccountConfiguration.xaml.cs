﻿using Lib.Security;
using Mailer.Controllers;
using Mailer.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mailer.Views.UserControls
{
    /// <summary>
    /// Interaction logic for AccountConfiguration.xaml
    /// </summary>
    public partial class AccountConfiguration : UserControl
    {
        AccountConfigurationController acc;
        public AccountConfiguration()
        {
            InitializeComponent();
            ShowSettings();
            SetupController();
        }
        public void SetupController()
        {
            acc = new AccountConfigurationController(this);
            RoutedEventHandler routed = new RoutedEventHandler(acc.AccountConfigurationEventHandler);
            OkButton.Click += routed;
        }
        public void ShowSettings()
        {
            try
            {
                string phrase = Environment.MachineName + Environment.UserName;
                SMTPMailServerTextBox.Text = Settings.Default.SMTPMailServer;
                IncomingMailPortTextBox.Text = Settings.Default.IncomingMailPort.ToString();
                OutgoingMailPortTextBox.Text = Settings.Default.OutgoingMailPort.ToString();
                DisplayNameTextBox.Text = Settings.Default.DisplayName;
                MailAddressTextBox.Text = Settings.Default.MailAddress;
                UserIDTextBox.Text = Settings.Default.UserID;
                PasswordTextBox.Password = StringCipher.Decrypt(Settings.Default.Password, phrase);
                SSLModeCheckBox.IsChecked = Settings.Default.SSLMode;
            }
            catch (Exception ex) { Console.WriteLine(ex.StackTrace); }
        }
        public void SaveSettings()
        {
            try
            {
                string phrase = Environment.MachineName + Environment.UserName;
                Settings.Default.SMTPMailServer = SMTPMailServerTextBox.Text.Trim();
                Settings.Default.IncomingMailPort = int.Parse(IncomingMailPortTextBox.Text);
                Settings.Default.OutgoingMailPort = int.Parse(OutgoingMailPortTextBox.Text);
                Settings.Default.DisplayName = DisplayNameTextBox.Text.Trim();
                Settings.Default.MailAddress = MailAddressTextBox.Text.Trim();
                Settings.Default.UserID = UserIDTextBox.Text.Trim();
                Settings.Default.Password = StringCipher.Encrypt(PasswordTextBox.Password.Trim(), phrase);
                Settings.Default.SSLMode = (bool)SSLModeCheckBox.IsChecked;
            }
            catch (Exception ex) { Console.WriteLine(ex.StackTrace); }
            Settings.Default.Save();
        }

    }
}
