﻿using Mailer.Views.UserControls;
using System.Windows;
using System.Windows.Controls;

namespace Mailer.Controllers
{
    public class AccountConfigurationController
    {
        private AccountConfiguration accountConfiguration;
        public AccountConfigurationController(AccountConfiguration  accountConfiguration)
        {
            this.accountConfiguration = accountConfiguration;
        }
        public void AccountConfigurationEventHandler(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            switch (button.Name)
            {
                case "OkButton":
                    accountConfiguration.SaveSettings();
                    break;
                case "CancelButton":
                    break;
            }
        }
    }
}
