/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wssoftware.demonb3.controllers;

import com.wssoftware.demonb3.views.CalendarFrame;
import com.wssoftware.demonb3.views.DemoCComboBox;
import com.wssoftware.demonb3.views.EditorFrame;
import com.wssoftware.demonb3.views.FileExplorerFrame;
import com.wssoftware.demonb3.views.MainFrame;
import com.wssoftware.demonb3.views.PersonDMForm;
import com.wssoftware.demonb3.views.ProductFrame;
import com.wssoftware.demonb3.views.ProductListFrame;
import com.wssoftware.demonb3.views.SettingsDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author willj
 */
public class MainController implements ActionListener{
    MainFrame mainframe;
    public MainController(MainFrame mainframe){
        this.mainframe = mainframe;
    }
    @Override
    public void actionPerformed(ActionEvent event) {
        switch(event.getActionCommand()){
            case "editor":
                showEditorFrame();
                break;
            case "calendar":
                showCalendarFrame();
                break;
            case "product":
                showProductListFrame();
                break;
            case "person":
                showPersonFrame();
                break;
            case "fileExplorer":
                showFileExplorerFrame();
                break;
            case "CComboBox":
                showCComboBox();
                break;
            case "configurations":
                showSettingsDialog();
                break;
            case "exit":
                System.exit(0);
                break;
        }
    }
    public void showSettingsDialog(){
        SettingsDialog sd = new SettingsDialog(mainframe, true);
        sd.setLocationRelativeTo(null);
        sd.setVisible(true);
    }
    public void showFileExplorerFrame(){
        FileExplorerFrame f = new FileExplorerFrame();
        mainframe.showChild(f, true);
    }   
    public void showPersonFrame(){
        PersonDMForm p = new PersonDMForm();
         mainframe.showChild(p, false);
    }
    public void showCalendarFrame(){
        CalendarFrame c = new CalendarFrame();
        mainframe.showChild(c, false);
    }
    public void showEditorFrame(){
        EditorFrame e = new EditorFrame(EditorFrame.TXT_FILE);
        mainframe.showChild(e, true);
    }   
        private void showProductListFrame(){
        ProductListFrame plf = new ProductListFrame();
        mainframe.showChild(plf, true);
    }
    private void showProductFrame(){
        ProductFrame pf = new ProductFrame();
        mainframe.showChild(pf, false);
    }

    private void showCComboBox() {
        DemoCComboBox comboBox = new DemoCComboBox();
        mainframe.showChild(comboBox, false);
    }
}
