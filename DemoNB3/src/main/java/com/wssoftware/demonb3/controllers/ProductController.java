/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wssoftware.demonb3.controllers;

import com.wssoftware.demonb3.models.Product;
import com.wssoftware.demonb3.models.ProductTableModel;
import com.wssoftware.demonb3.models.ProductTableModelList;
import com.wssoftware.demonb3.views.ProductFrame;
import com.wssoftware.demonb3.views.ProductListFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JTextField;

/**
 *
 * @author willj
 */
public class ProductController extends KeyAdapter implements ActionListener {

    private ProductFrame productframe;
    private JFileChooser dialog;
    private ProductListFrame listFrame;

    public ProductController(ProductFrame pf) {
        productframe = pf;
        dialog = new JFileChooser();
    }

    public ProductController(ProductListFrame listFrame) {
        dialog = new JFileChooser();
        this.listFrame = listFrame;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "save":
                save();
                break;
            case "clear":
                clear();
                break;
            case "select":
                select();
                break;
            case "open":
                showProductsOnTable();
                break;
            case "newRow":
                listFrame.addRow();
                break;
            case "saveTableContent":
                saveTableContent();
                break;
            case "deleteRow":
                listFrame.removeRow();
                break;
            case "close":
                listFrame.dispose();
                break;
        }
    }

    private void save() {
        dialog.showSaveDialog(productframe);
        if (dialog.getSelectedFile() != null) {
            writeFile(dialog.getSelectedFile(), productframe.getData());
        }

    }

    private void clear() {
        productframe.clear();
    }

    private void select() {
        dialog.showOpenDialog(productframe);
        if (dialog.getSelectedFile() != null) {
            Product p = readFile(dialog.getSelectedFile());
            productframe.showData(p);
        }
    }

    private void writeFile(File file, Product product) {
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(product);
            w.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Product readFile(File file) {
        Product product = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            product = (Product) in.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

    private Product[] readProductList(File file) {
        Product products[] = null;
        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(file))) {
            products = (Product[]) input.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products;
    }

    /**
     *  CAMBIO DE MODELO EN LA LINEA 145
     */
    private void showProductsOnTable() {
        dialog.showOpenDialog(productframe);
        File file = dialog.getSelectedFile();
        if (file != null) {
            listFrame.setProductListFile(file.getPath());
            //listFrame.setProductTableModel(new ProductTableModel(readProductList(file)));
            listFrame.setProductTableModel(new ProductTableModelList(readProductList(file)));
        }
    }

    private void saveTableContent() {
        dialog.showSaveDialog(productframe);
        Product products[] = listFrame.getDataTable();
        try (ObjectOutputStream output
                = new ObjectOutputStream(new FileOutputStream(dialog.getSelectedFile()))) {
            output.writeObject(products);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        JTextField input = (JTextField) e.getSource();
        switch (input.getName()) {
            case "price":
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE 
                        || c == KeyEvent.VK_ENTER || c == KeyEvent.VK_PERIOD)) {
                    e.consume();
                }
                break;
            case "productName":
                if (input.getText().length() >= 10) {
                    e.consume();
                }
                break;
        }
    }
}
