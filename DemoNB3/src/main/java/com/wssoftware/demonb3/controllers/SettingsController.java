/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wssoftware.demonb3.controllers;

import com.wssoftware.demonb3.misc.Constants;
import com.wssoftware.demonb3.misc.PropertyFile;
import com.wssoftware.demonb3.views.SettingsDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author wsanchez
 */
public class SettingsController implements ActionListener {

    private final SettingsDialog settingsDialog;
    private final String directory;

    
    public SettingsController(SettingsDialog settingsDialog) {
        this.settingsDialog = settingsDialog;
        PropertyFile prop = new PropertyFile();
        prop.open();
        if(prop.isFileExists()){
            this.directory = prop.getProperty(Constants.DIRECTORY_PROPERTY);
            this.settingsDialog.setSelectedDirectory(this.directory);
            this.settingsDialog.setCompanyName(prop.getProperty(Constants.COMPANYNAME_PROPERTY));
        }
        else{
            this.directory = "";
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "choose":
                chooseDirectory();
                break;
            case "ok":
                saveSettings();
                break;
        }
    }
    /**
     * Muestra una instancia de la clase JFileChooser, pero se configura para que solamente se selecionen carpetas.
     */
    public void chooseDirectory(){
        JFileChooser chooser = new JFileChooser(); 
        if(this.directory.isEmpty()){
            chooser.setCurrentDirectory(new java.io.File("."));
        }
        else{
            chooser.setCurrentDirectory(new java.io.File(this.directory));
        }
        chooser.setDialogTitle("Selecciona una carpeta");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //
        // disable the "All files" option.
        //
        chooser.setAcceptAllFileFilterUsed(false);
        //    
        if (chooser.showOpenDialog(settingsDialog) == JFileChooser.APPROVE_OPTION) {
            settingsDialog.setSelectedDirectory(chooser.getSelectedFile().getAbsolutePath());
        }
        else {
            if(settingsDialog.getSelectedDirectory().isEmpty()){
                settingsDialog.setSelectedDirectory("No ha seleccionado una carpeta.");
            }
        }     
    }
    /**
     * Guarda los valores configurados en un archivo llamado config.properties que estará ubicado en la ruta de la aplicación.
     */
    public void saveSettings(){
        PropertyFile prop = new PropertyFile();
        prop.setProperty(Constants.DIRECTORY_PROPERTY, settingsDialog.getSelectedDirectory());
        prop.setProperty(Constants.COMPANYNAME_PROPERTY, settingsDialog.getCompanyName());
        if(prop.saveProperties()){
            settingsDialog.dispose();
        }
        else{
            JOptionPane.showMessageDialog(settingsDialog, "Ocurrió un error al guardar la configuración.", "Configuración", JOptionPane.ERROR_MESSAGE);
        }
    }
    
}
