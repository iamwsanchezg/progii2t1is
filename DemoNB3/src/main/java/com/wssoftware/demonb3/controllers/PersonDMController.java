/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package com.wssoftware.demonb3.controllers;
import com.wssoftware.demonb3.models.Person;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import com.wssoftware.demonb3.views.PersonDMForm;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


/**
 *
 * @author William Sanchez
 */
public class PersonDMController implements ActionListener, FocusListener{
    PersonDMForm personFrame;
    JComboBox dComboBox;
    JComboBox mComboBox;
    JFileChooser d;
    Person person;
    String[] departments = new String[] { 
        "Boaco","Carazo","Chinandega","Chontales",
        "Costa Caribe Norte","Costa Caribe Sur",
        "Estelí","Granada","Jinotega","León",
        "Madriz","Managua","Masaya","Matagalpa",
        "Nueva Segovia","Río San Juan","Rivas" };
    String[] masayaMunicipalities = new String[] { 
        "Masaya","Nindirí", "Catarina", "Masatepe", 
        "La concepción", "Tisma","Nandasmo", 
        "San Juan de Oriente" };
    String[] managuaMunicipalities = new String[] { 
        "Managua","Ciudad Sandino", "El crucero", 
        "Mateare", "San Francisco Libre", 
        "San Rafael del Sur","Ticuantepe", "Tipitapa" };
    String[] granadaMunicipalities = new String[] { 
        "Granada","Diriomo", "Diriá", "Nandaime"};
    String[] leonMunicipalities = new String[] { 
        "León","Achuapa", "El Jicaral", 
        "La Paz Centro","Larreynaga",
        "Nagarote","Quezalguaque"};
    String[] rivasMunicipalities = new String[] { 
        "Rivas","Altagracia", "Belén", "Buenos Aires",
        "Cárdenas","Moyogalpa","Potosí", "San Jorge"};
    DefaultComboBoxModel departmentComboBoxModel = new DefaultComboBoxModel<>(departments);
    
    public PersonDMController(PersonDMForm f){
        super();
        personFrame = f;
        d = new JFileChooser();
        person = new Person();
    }
   
    public void setPerson(Person b){
        person = b;
    }
    public void setPerson(String filePath){
        File f = new File(filePath);
        readPerson(f);
    }
    public Person getPerson(){
        return person;
    }     
    public void setDepartmentComboBox(JComboBox jcombo){
        dComboBox = jcombo;
        dComboBox.setModel(departmentComboBoxModel);
    }
    public void setMunicipalityComboBox(JComboBox jcombo){
        mComboBox = jcombo;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().getClass().getName().equals(JComboBox.class.getName()))
        {
            JComboBox cbo = (JComboBox)e.getSource();
            switch(cbo.getName()){
                case "department":
                    switch(cbo.getSelectedItem().toString()){
                        case "Managua":
                            mComboBox.setModel(new DefaultComboBoxModel(managuaMunicipalities));
                            break;
                        case "Masaya":
                            mComboBox.setModel(new DefaultComboBoxModel(masayaMunicipalities));
                            break;
                        case "Granada":
                            mComboBox.setModel(new DefaultComboBoxModel(granadaMunicipalities));
                            break;
                        case "León":
                            mComboBox.setModel(new DefaultComboBoxModel(leonMunicipalities));
                            break;
                        case "Rivas":
                            mComboBox.setModel(new DefaultComboBoxModel(rivasMunicipalities));
                            break;
                        default:
                            mComboBox.setModel(new DefaultComboBoxModel(new String[]{}));
                            break;
                    }
                    break;
            }
        }
        else
        {
        switch (e.getActionCommand()){
            case "save":
                d.showSaveDialog(personFrame); 
                person = personFrame.getPersonData();
                writePerson(d.getSelectedFile());
                break;
            case "select":                
                d.showOpenDialog(personFrame);  
                person = readPerson(d.getSelectedFile());
                personFrame.setPersonData(person);
                break;
            case "clear":
                personFrame.clear();
                break;

        }
        }
    }
    private void writePerson(File file){
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getPerson());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(PersonDMController.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    private Person readPerson(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Person)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(personFrame, e.getMessage(),personFrame.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(PersonDMController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private Person[] readPersonList(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
        Person[] persons;
        try(FileInputStream in = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(in)) {
            persons = (Person[]) s.readObject();
        }
        return persons;
    }
    @Override
    public void focusGained(FocusEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent e) {
        if(e.getOppositeComponent().getClass().getName().endsWith("JTextField")){
            switch(((javax.swing.JTextField) e.getSource()).getName()){
                case "idTextField":
                    System.out.println("Evento capturarado desde: idTextField" + ", valor: " +((javax.swing.JTextField) e.getSource()).getText());
                    break;
                default:
                    System.out.println("Evento captudarado desde: " + ((javax.swing.JTextField) e.getSource()).getName());
                    break;
            }
        }
    }
}
