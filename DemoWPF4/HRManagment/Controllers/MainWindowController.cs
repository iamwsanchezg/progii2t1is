﻿using Entities.Models;
using HRManagment.ViewModels;
using HRManagment.Views.Windows;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace HRManagment.Controllers
{
    public class MainWindowController
    {
        private MainWindow mainWindow;
        public MainWindowController(MainWindow window)
        {
            mainWindow = window;
        }
        public void MainWindowEventHandler(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }
        public void MainMenuEventHandler(object sender, RoutedEventArgs e)
        {
            MenuItem Option = (MenuItem)sender;
            switch (Option.Name)
            {
                case "exitAplicationMenutItem":
                    Application.Current.Shutdown();
                    break;
                case "PersonMenuItem":
                    mainWindow.DataContext = new PersonViewModel();
                    break;
                case "PersonListMenuItem":
                    mainWindow.DataContext = new GroupViewModel() { Title = "Spiderman and the fantastic 4", Group = GetGroup() };
                    break;
                case "SettingsMenutItem":
                    mainWindow.DataContext = new SettingsViewModel();
                    break;

            }
        }
        private Group GetGroup()
        {
            Group g = new Group
            {
                Name = "Los 4 Fantásticos",
                Members = new List<Person>
                    {
                        new Person() { Names = "Peter", LastNames = "Parker", BirthDate = new DateTime(2000, 1, 1), State = "New York" },
                        new Person() { Names = "Susan", LastNames = "Storm", BirthDate = new DateTime(1985, 1, 1), State = "New York" },
                        new Person() { Names = "Johny", LastNames = "Storm", BirthDate = new DateTime(1985, 1, 1),State="New York" },
                        new Person() { Names = "Reed", LastNames = "Richards", BirthDate = new DateTime(1980, 1, 1), State="New York" }
                    }
            };
            return g;
        }
    }
}
