﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Misc
{
    public static class Util
    {
        public static int YearsFrom(this DateTime inputDate)
        {
            int Years = DateTime.Now.Year - inputDate.Year;
            int InitMonth = inputDate.Month;
            int Daybd = inputDate.Day;

            int month = DateTime.Now.Month;
            int day = DateTime.Now.Day;

            if (month < InitMonth)
            {
                Years--;
            }
            else
            {
                if (month == InitMonth)
                {
                    if (day < Daybd)
                    {
                        Years--;
                    }
                }
            }
            return Years;
        }
    }
}
