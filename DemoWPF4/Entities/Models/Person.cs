﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Interfaces;
using Lib.Misc;
using Lib.Serialization;

namespace Entities.Models
{
    [Serializable]
    public class Person: IWSFile<Person>
    {
        public string Names { get; set; }
        public string LastNames { get; set; }
        public DateTime BirthDate { get; set; }
        public int Age { get { return BirthDate.YearsFrom(); } }
        public string State { get; set; }

        public Person FromJson(string Path)
        {
            return JsonSerialization.ReadFromJsonFile<Person>(Path);
        }

        public void ToJson(string Path)
        {
            JsonSerialization.WriteToJsonFile(Path, this, false);
        }
    }
}
