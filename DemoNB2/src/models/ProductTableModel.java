/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.lang.reflect.Field;
import java.util.Date;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author willj
 */
public class ProductTableModel extends AbstractTableModel{
    private Object columnNames[];
    private Object rowData[][];
    Class[] types;
    boolean[] canEdit;

    public ProductTableModel() {
        setup();
    }
    public ProductTableModel(Product[] productList){
        setup();
       rowData = new Object[productList.length][columnNames.length];
       int c=0;
       for(Product product:productList){
           rowData[c] = new Object[]{
               product.getProductId(),
               product.getProductName(),
               product.getPrice(),
               product.getExpireDate()
           };
           
           c++;
       }
       setDataModel(rowData);
    }
    private void setup(){
        Field fields[] = Product.class.getDeclaredFields();
        columnNames = new String[fields.length];
        types = new Class[fields.length];
        canEdit = new boolean[fields.length];
        int c=0;
        for(Field field:fields){
            columnNames[c] =field.getName();
            types[c] = field.getType();
            canEdit[c] = true;
            c++;
        }
    }
    
    public TableModel getModel(){
        TableModel model = new DefaultTableModel(rowData,columnNames)
        {
            @Override
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        };
        return model;
    }
    public void setDataModel(Object[][] data){
        rowData = data;
    }
    public Product[] getData(){
        Product [] productList = null;
        if(rowData!=null){
            productList = new Product[rowData.length];
            for(int i=0;i<rowData.length;i++){
                Product p = new Product();
                p.setProductId((int)rowData[i][0]);
                p.setProductName((String)rowData[i][1]);
                p.setPrice((double)rowData[i][2]);
                p.setExpireDate((Date)rowData[i][3]);
                
                productList[i] = p;
            }
        }
        else
        {
            productList = new Product[0];
        }
        return productList;
    }
    
    @Override
    public int getRowCount() {
        if(rowData==null){
            return 0;
        }
        else
        {
            return rowData.length;
        }
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return rowData[rowIndex][columnIndex];
    }
    
}
