/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import models.CustomFile;
import views.MainFrame;
import views.Notepad;
import views.ProductFrame;
import views.ProductListFrame;

/**
 *
 * @author Sistemas-08
 */
public class MainController implements ActionListener{
    static int windowsCount = 0;
    private MainFrame frame;
    private JFileChooser fc; 
    public MainController(){
        frame = new MainFrame();
        fc = new JFileChooser();
    }
    public MainController(MainFrame f){
        frame = f;
        fc = new JFileChooser();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand())
        {
            case "new":
                showNewForm();
                break;
            case "open":
                showOpenFileDialog();
                break;
            case "save":
                showSaveFileDialog();
                break;
            case "cut":
                break;
            case "copy":
                break;
            case "paste":
                break; 
            case "product":
//                showProductListFrame();
                showProductFrame();
                break;
            case "exit":
                System.exit(0);
                break;
        }
    }
    private void showProductListFrame(){
        ProductListFrame plf = new ProductListFrame();
        frame.desktop.add(plf);
        plf.setVisible(true);
        frame.desktop.getDesktopManager().maximizeFrame(plf);
    }
    private void showProductFrame(){
        ProductFrame pf = new ProductFrame();
        frame.desktop.add(pf);
        pf.setVisible(true);
    }
    private void showNewForm()
    {
            windowsCount++;
            String title = "Sin título " + String.valueOf(windowsCount);
            Notepad n = new Notepad(title);
            frame.desktop.add(n);
            n.setVisible(true);
            frame.desktop.getDesktopManager().maximizeFrame(n);
    }
    private void showFileInForm(CustomFile customfile)
    {
            Notepad n = new Notepad(customfile);
            frame.desktop.add(n);
            n.setVisible(true);
            frame.desktop.getDesktopManager().maximizeFrame(n);
    }
    private File showOpenFileDialog(){
        File file = null;
        if (fc.showOpenDialog(frame)==JFileChooser.APPROVE_OPTION){
            file = fc.getSelectedFile();
            if(file!=null){
                try {
                    readFile(file);
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
               JOptionPane.showMessageDialog(frame, "No se seleccionó archivo");
            }
        }
        return file;
    }
    
    private void readFile(File file) throws FileNotFoundException, IOException, ClassNotFoundException {
        
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            CustomFile myCustomFile = (CustomFile) ois.readObject();
            myCustomFile.setFileName(file.getName());
            //JOptionPane.showMessageDialog(frame, myCustomFile.getContent());
            showFileInForm(myCustomFile);
        
    }
    private void showSaveFileDialog(){
       File file;
       if (fc.showSaveDialog(frame)==JFileChooser.APPROVE_OPTION){
            file = fc.getSelectedFile();
            if(file!=null){
                try {                    
                    writeFile(file, getDataFromForm());
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
               JOptionPane.showMessageDialog(frame, "No se seleccionó archivo", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }        
    }
    
    private CustomFile getDataFromForm(){
        JInternalFrame [] jiframes = frame.desktop.getAllFrames();
        CustomFile cf = null;        
        for(int i=0; i<jiframes.length; i++){
            if(jiframes[i].isSelected())
            {
                if(jiframes[i].getClass().getName().equals(Notepad.class.getName())){
                    cf = ((Notepad)jiframes[i]).getData();
                }
            }
        }        
        return cf;
    }
    
    private void writeFile(File file, CustomFile myFile) throws IOException {      
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(myFile);
            w.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
