/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import models.Product;
import views.ProductFrame;

/**
 *
 * @author Sistemas-08
 */
public class ProductController implements ActionListener, KeyListener{
    private ProductFrame productframe;
    private JFileChooser dialog;
    public ProductController(ProductFrame pf) {
        productframe = pf;
        dialog = new JFileChooser();        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "save":
                save();
                break;
             case "clear":
                 clear();
                break;
             case "select":
                 select();
                break;               
        }
    }
    
    private void save(){
        dialog.showSaveDialog(productframe);
        if(dialog.getSelectedFile()!=null){
          writeFile(dialog.getSelectedFile(), productframe.getData());
        }

    }
    private void clear(){
        productframe.clear();
    }
    private void select(){
        dialog.showOpenDialog(productframe);
        if(dialog.getSelectedFile()!=null){
            Product p = readFile(dialog.getSelectedFile());
            productframe.showData(p);
        }
    }
    
    private void writeFile(File file, Product product){
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(product);
            w.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Product readFile(File file){
        Product product = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            product = (Product)in.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        JTextField input = (JTextField)e.getSource();
        switch(input.getName()){
            case "price":
                char c = e.getKeyChar();
                if(!(Character.isDigit(c)|| c==KeyEvent.VK_BACK_SPACE|| c==KeyEvent.VK_ENTER|| c==KeyEvent.VK_PERIOD)){
                   e.consume(); 
                }
                break;
            case "productName":
                if(input.getText().length()>=10){
                    e.consume();
                }
                break;
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
