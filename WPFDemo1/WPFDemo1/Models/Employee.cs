﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFDemo1.Models
{
    public class Employee:Person
    {
        public string EmployeeNumber { get; set; }
        public string SocialSecurityNumber { get; set; }
        public double Wage { get; set; }
        public int WorkedDays { get; set; }
        public double getSalary()
        {
            double salary = Wage / 30 * WorkedDays;
            return salary;
        }
    }
}
